package com.cxs.videoplayer.util;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.TextAlignment;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;

import static ohos.agp.components.ComponentContainer.LayoutConfig.MATCH_CONTENT;
import static ohos.agp.components.ComponentContainer.LayoutConfig.MATCH_PARENT;


public class DialogUtils {
    private DialogUtils() {}
    public static void showTip(Context context, String msg, int durationTime) {
        // 提示框的核心组件文本
        Text text = new Text(context);
        text.setWidth(MATCH_CONTENT);
        text.setHeight(MATCH_CONTENT);
        text.setTextSize(16, Text.TextSizeType.FP);
        text.setText(msg);
        text.setPadding(30, 20, 30, 20);
        text.setMultipleLine(true);
        text.setTextColor(Color.WHITE);
        text.setTextAlignment(TextAlignment.CENTER);

        // 给上面的文本设置一个背景样式
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setShape(ShapeElement.RECTANGLE);
        shapeElement.setRgbColor(new RgbColor(0x666666FF));
        shapeElement.setCornerRadius(15);
        text.setBackground(shapeElement);

        // 构建存放上面的text的布局
        DirectionalLayout directionalLayout = new DirectionalLayout(context);
        directionalLayout.setWidth(MATCH_PARENT);
        directionalLayout.setHeight(MATCH_CONTENT);
        directionalLayout.setAlignment(LayoutAlignment.CENTER);
        directionalLayout.addComponent(text);

        // 最后要让上面的组件绑定dialog
        ToastDialog toastDialog = new ToastDialog(context);
        toastDialog.setSize(MATCH_PARENT, MATCH_CONTENT);
        toastDialog.setDuration(durationTime);
        toastDialog.setAutoClosable(true);
        toastDialog.setTransparent(true);
        toastDialog.setAlignment(LayoutAlignment.CENTER);
        toastDialog.setComponent((Component)directionalLayout);
        toastDialog.show();
    }

    public static void showTip(Context context, String msg) {
        showTip(context, msg, 1000);
    }
}