package com.cxs.videoplayer.provider;

import com.cxs.videoplayer.ResourceTable;
import com.cxs.videoplayer.data.CommentInfo;
import ohos.agp.components.*;

import java.util.List;

public class CommentListProvider extends BaseItemProvider {

    List<CommentInfo> commentInfos;

    public CommentListProvider(List<CommentInfo> commentInfos) {
        this.commentInfos = commentInfos;
    }

    @Override
    public int getCount() {
        return commentInfos.size();
    }

    @Override
    public Object getItem(int i) {
        if (commentInfos != null && i < commentInfos.size()) {
            return commentInfos.get(i);
        }
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        final Component cpt;
        ViewHolder viewHolder;
        if (component == null) {
            cpt = LayoutScatter.getInstance(componentContainer.getContext())
                    .parse(ResourceTable.Layout_comment_list_item, null, false);
            viewHolder = new ViewHolder(cpt);
            cpt.setTag(viewHolder);
        } else {
            cpt = component;
            viewHolder = (ViewHolder)cpt.getTag();
        }
        if (viewHolder != null) {
            viewHolder.name.setText(commentInfos.get(i).getName());
            viewHolder.content.setText(commentInfos.get(i).getContent());
            viewHolder.date.setText(commentInfos.get(i).getSubDate());
        }
        return cpt;
    }

    private static class ViewHolder {
        Text name;
        Text content;
        Text date;

        public ViewHolder(Component component) {
            name = (Text)component.findComponentById(ResourceTable.Id_comment_name);
            content = (Text)component.findComponentById(ResourceTable.Id_comment_content);
            date = (Text)component.findComponentById(ResourceTable.Id_comment_date);
        }
    }
}
