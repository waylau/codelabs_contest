package com.cxs.videoplayer.data;

public class VideoInfo {
    int index;
    String url;
    String title;
    String thumbnail;
    String synopsis;

    public VideoInfo(int index, String url, String title, String thumbnail, String synopsis) {
        this.index = index;
        this.url = url;
        this.title = title;
        this.thumbnail = thumbnail;
        this.synopsis = synopsis;
    }

    public int getIndex() {
        return index;
    }

    public String getUrl() {
        return url;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public String getSynopsis() {
        return synopsis;
    }

    @Override
    public String toString() {
        return "VideoInfo{" +
                "index=" + index +
                ", url='" + url + '\'' +
                ", title='" + title + '\'' +
                ", thumbnail='" + thumbnail + '\'' +
                '}';
    }
}
