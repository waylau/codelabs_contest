package com.cxs.videoplayer.component;

import com.cxs.videoplayer.ResourceTable;
import com.cxs.videoplayer.data.CommentInfo;
import com.cxs.videoplayer.provider.CommentListProvider;
import com.cxs.videoplayer.slice.VideoPlayerAbilitySlice;
import com.cxs.videoplayer.util.RdbStoreUtils;
import com.cxs.videoplayer.util.DialogUtils;
import ohos.accessibility.ability.AccessibleAbility;
import ohos.accessibility.ability.SoftKeyBoardController;
import ohos.accessibility.ability.SoftKeyBoardListener;
import ohos.agp.components.*;
import ohos.agp.window.service.WindowManager;
import ohos.app.Context;
import ohos.data.rdb.RdbPredicates;
import ohos.data.rdb.RdbStore;
import ohos.data.rdb.ValuesBucket;
import ohos.data.resultset.ResultSet;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CommentListView {

    List<CommentInfo> commentInfos;
    Context context;
    RdbStore rdbStore;
    ListContainer listContainer;
    CommentListProvider commentListProvider;

    public CommentListView(Context context) {
        this.context = context;
        rdbStore = RdbStoreUtils.getRdbStore(context);
        commentInfos = new ArrayList<>();
    }

    public DirectionalLayout getView() {
        DirectionalLayout layout = (DirectionalLayout) LayoutScatter.getInstance(context)
                .parse(ResourceTable.Layout_comment_list, null, false);
        TextField input = ((TextField)layout.findComponentById(ResourceTable.Id_tf_input));

        layout.findComponentById(ResourceTable.Id_btn_send)
                .setClickedListener(c -> {
                    writeData(input);
                });

        listContainer = (ListContainer) layout.findComponentById(ResourceTable.Id_comment_listcontainer);
        commentListProvider = new CommentListProvider(getData());
        listContainer.setItemProvider(commentListProvider);

        return layout;
    }

    private List<CommentInfo> getData() {
        RdbPredicates rdbPredicates = new RdbPredicates("table_test")
                .orderByAsc("sub_date");
        String[] cols = {"name", "content", "sub_date"};
        ResultSet query = rdbStore.query(rdbPredicates, cols);
        while (query.goToNextRow()) {
            String name = query.getString(query.getColumnIndexForName("name"));
            String content = query.getString(query.getColumnIndexForName("content"));
            String date = query.getString(query.getColumnIndexForName("sub_date"));
            commentInfos.add(new CommentInfo(name, content, date));
        }

        return commentInfos;
    }

    private void writeData(TextField textField) {
        String name = "chaoxiaoshu";
        String input = textField.getText();
        if (input == null || input.equals("")) {
            DialogUtils.showTip(context, "输入不能为空");
            return;
        }
        String date = getDate();

        ValuesBucket vb = new ValuesBucket();
        vb.putString("name", name);
        vb.putString("content", input);
        vb.putString("sub_date", date);
        long rowId = rdbStore.insert("table_test", vb);

        commentInfos.add(new CommentInfo(name, input, date));
        commentListProvider.notifyDataChanged();

        textField.setText("");
        DialogUtils.showTip(context, "发布成功");
    }

    private String getDate() {
        Date dNow = new Date( );
        SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd");
        return ft.format(dNow);
    }

}
