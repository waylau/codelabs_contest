package com.cxs.videoplayer.util;

import ohos.app.Context;
import ohos.data.distributed.common.KvManagerConfig;
import ohos.data.distributed.common.KvManagerFactory;
import ohos.distributedschedule.interwork.DeviceInfo;
import ohos.distributedschedule.interwork.DeviceManager;

import java.util.*;

public class DeviceUtils {
    private DeviceUtils () {}

    // 获取远程设备列表
    public static Map<String, String> getDeviceList() {
        // 第一步：获取设备id
        List<DeviceInfo> deviceList = DeviceManager.getDeviceList(DeviceInfo.FLAG_GET_ONLINE_DEVICE);
        // 判断组网设备是否为空
        if (deviceList.isEmpty()) {
            return null;
        }
        int deviceNum = deviceList.size();
        List<String> deviceIds = new ArrayList<>(deviceNum);
        List<String> deviceNames = new ArrayList<>(deviceNum);
        Map<String, String> map = new HashMap<>();
        deviceList.forEach((device)->{
            map.put(device.getDeviceId(), device.getDeviceName());
        });
        return map;
    }


    // 获取远程设备Id
    public static String getDeviceId() {
        Map<String, String> map = getDeviceList();
        Optional<String> first = map.keySet().stream().findFirst();

        return first.get();
    }

    // 获取本地设备Id
    public static String getLocalDeviceId(Context context) {
        String localDeviceId = KvManagerFactory.getInstance()
                .createKvManager(new KvManagerConfig(context)).getLocalDeviceInfo().getId();
        return localDeviceId;
    }

    // 获取本地设备Type
    public static String getLocalDeviceType(Context context) {
        String localDeviceType = KvManagerFactory.getInstance()
                .createKvManager(new KvManagerConfig(context)).getLocalDeviceInfo().getType();
        return localDeviceType ;
    }

    public static String getLocalDeviceName(Context context) {
        String localDeviceName = KvManagerFactory.getInstance()
                .createKvManager(new KvManagerConfig(context)).getLocalDeviceInfo().getName();
        return localDeviceName ;
    }
}
