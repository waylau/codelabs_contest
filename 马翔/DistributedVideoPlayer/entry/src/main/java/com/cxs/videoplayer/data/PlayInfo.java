package com.cxs.videoplayer.data;

import com.cxs.videoplayer.constant.ControllCode;

public class PlayInfo {

    private VideoInfo videoInfo;
    private ControllCode status;
    private long currentTime;

    public PlayInfo(VideoInfo videoInfo, ControllCode status, long currentTime) {
        this.videoInfo = videoInfo;
        this.status = status;
        this.currentTime = currentTime;
    }

    public VideoInfo getVideoInfo() {
        return videoInfo;
    }

    public ControllCode getStatus() {
        return status;
    }

    public long getCurrentTime() {
        return currentTime;
    }
}
