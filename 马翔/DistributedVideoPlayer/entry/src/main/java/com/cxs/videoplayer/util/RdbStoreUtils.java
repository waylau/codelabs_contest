package com.cxs.videoplayer.util;

import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.rdb.RdbOpenCallback;
import ohos.data.rdb.RdbStore;
import ohos.data.rdb.StoreConfig;

public class RdbStoreUtils {

    private RdbStoreUtils() {}

    public static RdbStore getRdbStore(Context context) {
        DatabaseHelper helper = new DatabaseHelper(context);

        // 准备数据库配置信息，简单起见只配置数据库名字。其他都为默认值
        StoreConfig storeConfig = StoreConfig.newDefaultConfig("RdbStoreTest.db");

        RdbOpenCallback callback = new RdbOpenCallback() {
            @Override
            public void onCreate(RdbStore rdbStore) {
                // 创建数据库时被回调，可以在该方法中初始化表结构，并添加一些应用使用到的初始化数据
                rdbStore.executeSql("CREATE TABLE IF NOT EXISTS table_test" +
                        "(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL, content TEXT NOT NULL, sub_date TEXT NOT NULL)");
            }

            @Override
            public void onUpgrade(RdbStore rdbStore, int i, int i1) {

            }
        };

        // 通过helper的方法，来获取到数据库对象,getRdbStore()：如果数据库不存在，自动创建
        RdbStore rdbStore = helper.getRdbStore(storeConfig, 1, callback, null);
        return rdbStore;
    }
}
