package com.cxs.videoplayer.player;

import com.cxs.videoplayer.data.VideoInfo;
import ohos.agp.graphics.Surface;

import java.util.List;

public interface IHmPlayer {

    void setSurface(Surface surface);

    void setVideo(VideoInfo videoInfo);

    boolean prepare();

    void start(long position);

    void play();

    void pause();

    void stop();

    void release();

    long getDuration();

    void rewindTo(long currentTime);

    long getCurrentTime();

    void setVideoList(List<VideoInfo> videoInfoList);
}
