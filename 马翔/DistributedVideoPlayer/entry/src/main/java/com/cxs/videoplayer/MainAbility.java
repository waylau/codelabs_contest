package com.cxs.videoplayer;

import com.cxs.videoplayer.slice.VideoPlayerAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.IAbilityContinuation;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.IntentParams;

public class MainAbility extends Ability implements IAbilityContinuation {

    private String[] permissions = new String[]{
            "ohos.permission.DISTRIBUTED_DATASYNC"
    };

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(VideoPlayerAbilitySlice.class.getName());

        requestPermissionsFromUser(permissions, 0);
    }

    @Override
    public boolean onStartContinuation() {
        return true;
    }

    @Override
    public boolean onSaveData(IntentParams intentParams) {
        return true;
    }

    @Override
    public boolean onRestoreData(IntentParams intentParams) {
        return true;
    }

    @Override
    public void onCompleteContinuation(int i) {

    }
}
