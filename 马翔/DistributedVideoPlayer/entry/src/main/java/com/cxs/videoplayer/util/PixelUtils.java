package com.cxs.videoplayer.util;

import ohos.agp.components.AttrHelper;
import ohos.app.Context;

public class PixelUtils {

    private PixelUtils(){}

    public static int vp2px(Context context, float vp) {
        return AttrHelper.vp2px(vp, context);
    }
}
