package com.example.videoplayer.newsdata;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;

import ohos.app.Context;
import ohos.global.resource.Resource;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.utils.zson.ZSONObject;

/**
 * newsInfo class, defined the newsInfo structure, and provide related method to read newsInfo from json.
 * You can override this part to extend business logic, such as read from different data source
 */
public class NewsInfo {
    private static final HiLogLabel TAG = new HiLogLabel(0, 0, "NewsInfo");
    private static final String NEWS_FILE_PREFIX = "resources/rawfile/news";
    private static final int NEWS_LIMIT = 10;
    private static final int NEWS_CONTENT_SIZE = 1024;
    private static final int EOF = -1;
    private volatile static NewsInfo newsInfo;
    private final Context context;
    private final HashMap<String, String> newsInfoMap = new HashMap<>();
    private final ArrayList<String> newsTitleList = new ArrayList<>();
    private final ArrayList<String> newsUrlList = new ArrayList<>();
    private final ArrayList<String> newsImageList = new ArrayList<>();
    private final ArrayList<Boolean> newsContainsVideos = new ArrayList<>();
    private final ArrayList<String> newsVideoUrl = new ArrayList<>();
    private final ArrayList<Boolean> newsContainsWeb = new ArrayList<>();
    private final ArrayList<String> newsWebList = new ArrayList<>();

    /**
     * default constructor
     *
     * @param context context
     */
    private NewsInfo(Context context) {
        this.context = context;
        int index = 0;
        while (index < NEWS_LIMIT) {
            String resourcePath = NEWS_FILE_PREFIX + index + ".json";
            String newsString = getNewsFile(resourcePath);
            if ("".equals(newsString)) {
                break;
            }
            ZSONObject newsJson = ZSONObject.stringToZSON(newsString);
            String url = newsJson.getString("url");
            String title = newsJson.getString("title");
            String image = newsJson.getString("title_image");
            boolean containsVideo = newsJson.getBooleanValue("contains_video");
            String video = newsJson.getString("title_video");
            boolean containsWeb = newsJson.getBooleanValue("contains_webpage");
            String webPage = newsJson.getString("web_page");

            newsUrlList.add(url);
            newsTitleList.add(title);
            newsImageList.add(image);
            newsInfoMap.put(url, resourcePath);
            newsContainsVideos.add(containsVideo);
            newsVideoUrl.add(video != null ? video : "");
            newsContainsWeb.add(containsWeb);
            newsWebList.add(webPage);
            index++;
        }
    }

    /**
     * singleton newsInfo
     *
     * @param context context.
     * @return newsInfo.
     */
    public static NewsInfo getInstance(Context context) {
        if (newsInfo == null) {
            synchronized (NewsInfo.class) {
                if (newsInfo == null) {
                    newsInfo = new NewsInfo(context);
                }
            }
        }
        return newsInfo;
    }

    /**
     * to check whether news contains video.
     *
     * @param index the news index.
     * @return whether news contains video.
     */
    boolean getNewsContainsVideos(int index) {
        return newsContainsVideos.get(index);
    }

    /**
     * get news video url
     *
     * @param index the news index.
     * @return empty String or video url.
     */
    String getNewsVideoUrl(int index) {
        return newsVideoUrl.get(index);
    }

    /**
     * getNewsCount
     *
     * @return newsList size
     */
    int getNewsCount() {
        return newsUrlList.size();
    }

    /**
     * getNewsTitle
     *
     * @param index news index
     * @return news title
     */
    public String getNewsTitle(int index) {
        return newsTitleList.get(index);
    }


    /**
     * get specific news url.
     *
     * @param index the news index.
     * @return specific news url.
     */
    public String getNewsUrl(int index) {
        return newsUrlList.get(index);
    }

    /**
     * getNewsImage
     *
     * @param index news index
     * @return new image url
     */
    public String getNewsImage(int index) {
        return newsImageList.get(index);
    }

    /**
     * getNewsJson
     *
     * @param url new url
     * @return invoke getNewsFile()
     */
    String getNewsJson(String url) {
        String path = newsInfoMap.get(url);
        return getNewsFile(path);
    }

    /**
     * readNewsFile from json, you can extend this part to read newsInfo from different data source.
     *
     * @param resourcePath news file location
     * @return newsJson
     */
    private String getNewsFile(String resourcePath) {
        try {
            Resource resource = context.getResourceManager().getRawFileEntry(resourcePath).openRawFile();
            byte[] tmp = new byte[NEWS_CONTENT_SIZE * NEWS_CONTENT_SIZE];
            int reads = resource.read(tmp);
            if (reads != EOF) {
                return new String(tmp, 0, reads, StandardCharsets.UTF_8);
            }
        } catch (IOException ex) {
            HiLog.error(TAG, "GetHttpNews get IO error");
        }
        return "";
    }

    /**
     * to check whether news contains video.
     *
     * @param index the news index.
     * @return whether news contains video.
     */
    boolean getNewsContainsWeb(int index) {
        return newsContainsWeb.get(index);
    }

    /**
     * get Web News url for Web View.
     *
     * @param index the news index.
     * @return News Http url.
     */
    public String getWebNewsUrl(int index) {
        return newsWebList.get(index);
    }
}
