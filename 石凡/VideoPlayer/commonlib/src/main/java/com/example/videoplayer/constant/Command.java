package com.example.videoplayer.constant;

/**
 * message_code which send to TV.
 */
public class Command {
    /**
     * the message code of tv command
     */
    public static final int COMMAND_TV = 1;
}