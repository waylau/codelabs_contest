package com.lzf.mytestnews;

import com.lzf.mytestnews.utils.BuriedPointEventImpl;
import com.yc.kernel.factory.PlayerFactory;
import com.yc.kernel.impl.ijk.IjkPlayerFactory;
import com.yc.music.utils.MusicSpUtils;
import com.yc.video.config.VideoPlayerConfig;
import com.yc.video.player.VideoViewManager;
import com.yc.videosqllite.manager.CacheConfig;
import com.yc.videosqllite.manager.LocationManager;
import ohos.aafwk.ability.AbilityPackage;

public class MyApplication extends AbilityPackage {
    @Override
    public void onInitialize() {
        super.onInitialize();


        //播放器配置，注意：此为全局配置，按需开启
        PlayerFactory player = IjkPlayerFactory.create();
        VideoViewManager.setConfig(VideoPlayerConfig.newBuilder()
                //设置上下文
                .setContext(this)
                //设置视频全局埋点事件
                .setBuriedPointEvent(new BuriedPointEventImpl())
                //调试的时候请打开日志，方便排错
                .setLogEnabled(true)
                //设置ijk
                .setPlayerFactory(player)
                //创建SurfaceView
                //.setRenderViewFactory(SurfaceViewFactory.create())
                .build());
        MusicSpUtils.init(this);

        initVideoCache();

    }


    private void initVideoCache() {
        CacheConfig cacheConfig = new CacheConfig();
        cacheConfig.setIsEffective(true);
        cacheConfig.setType(2);
        cacheConfig.setContext(this);
        cacheConfig.setCacheMax(1000);
        cacheConfig.setLog(false);
        LocationManager.getInstance().init(cacheConfig);
    }
}
