package com.lzf.mytestnews.slice;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lzf.mytestnews.ResourceTable;
import com.lzf.mytestnews.ability.VideoplayerAbility;
import com.lzf.mytestnews.bean.NewsInfo;
import com.lzf.mytestnews.provider.NewsListProvider;
import com.lzf.mytestnews.utils.CommonUtils;
import com.lzf.mytestnews.utils.DialogUtils;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.ListContainer;
import ohos.bundle.IBundleManager;

import java.util.ArrayList;
import java.util.List;

public class MainAbilitySlice extends AbilitySlice {

    private ListContainer newsListContainer;
    private List<NewsInfo> totalNewsDataList;
    private List<NewsInfo> newsDataList;
    private NewsListProvider newsListProvider;
    String PERMISSION = "ohos.permission.DISTRIBUTED_DATASYNC";

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        initView();
        initData();
        initListener();
        newsListContainer.setItemProvider(newsListProvider);
        newsListProvider.notifyDataChanged();


        if (verifySelfPermission(PERMISSION) != IBundleManager.PERMISSION_GRANTED) {
            requestPermissionsFromUser(new String[]{PERMISSION}, 0);
        }

    }

    private void initView() {
        newsListContainer = (ListContainer) findComponentById(ResourceTable.Id_news_container);
    }

    private void initData() {
        Gson gson = new Gson();
        totalNewsDataList =
                gson.fromJson(
                        CommonUtils.getStringFromJsonPath(this, "entry/resources/rawfile/news_datas.json"),
                        new TypeToken<List<NewsInfo>>() {
                        }.getType());
        newsDataList = new ArrayList<>();
        newsDataList.addAll(totalNewsDataList);
        newsListProvider = new NewsListProvider(newsDataList, this);
    }

    /**
     * init listener of news type and news detail
     */
    private void initListener() {

        newsListContainer.setItemClickedListener(
                (listContainer, component, position, id) -> {
//                    DialogUtils.toast(this, "点击" + position, 2000);
                    Intent intent = new Intent();
                    Operation operation =
                            new Intent.OperationBuilder()
                                    .withBundleName(getBundleName())
                                    .withAbilityName(VideoplayerAbility.class.getName())
                                    .withAction("action.detail")
                                    .build();
                    intent.setOperation(operation);
                    intent.setParam("index", position);
//                    intent.setParam(NewsDetailAbilitySlice.INTENT_LIKE, newsDataList.get(position).getLikes());
//                    intent.setParam(NewsDetailAbilitySlice.INTENT_CONTENT, newsDataList.get(position).getContent());
//                    intent.setParam(NewsDetailAbilitySlice.INTENT_IMAGE, newsDataList.get(position).getImgUrl());
                    startAbility(intent);
                });
    }


    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
