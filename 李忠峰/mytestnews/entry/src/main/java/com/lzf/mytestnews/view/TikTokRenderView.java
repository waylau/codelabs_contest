package com.lzf.mytestnews.view;

import com.yc.kernel.inter.AbstractVideoPlayer;
import com.yc.video.config.ConstantKeys;
import com.yc.video.surface.InterSurfaceView;
import ohos.agp.components.Component;
import ohos.agp.components.element.PixelMapElement;

/**
 * TikTok专用RenderView，横屏视频默认显示，竖屏视频居中裁剪
 * 使用代理模式实现
 *
 * @since 2021-05-12
 */
public class TikTokRenderView implements InterSurfaceView {
    private InterSurfaceView mProxyRenderView;

    TikTokRenderView(InterSurfaceView renderView) {
        this.mProxyRenderView = renderView;
    }

    @Override
    public void attachToPlayer(AbstractVideoPlayer player) {
        mProxyRenderView.attachToPlayer(player);
    }

    @Override
    public void setVideoSize(int videoWidth, int videoHeight) {
        if (videoWidth > 0 && videoHeight > 0) {
            mProxyRenderView.setVideoSize(videoWidth, videoHeight);
            if (videoHeight > videoWidth) {
                // 竖屏视频，使用居中裁剪
                mProxyRenderView.setScaleType(ConstantKeys.PlayerScreenScaleType.SCREEN_SCALE_CENTER_CROP);
            } else {
                // 横屏视频，使用默认模式
                mProxyRenderView.setScaleType(ConstantKeys.PlayerScreenScaleType.SCREEN_SCALE_DEFAULT);
            }
        }
    }

    @Override
    public void setVideoRotation(int degree) {
        mProxyRenderView.setVideoRotation(degree);
    }

    @Override
    public void setScaleType(int scaleType) {
        getView().getContext().getUITaskDispatcher().asyncDispatch(new Runnable() {
            @Override
            public void run() {
                mProxyRenderView.getView().postLayout();
            }
        });
    }

    @Override
    public Component getView() {
        return mProxyRenderView.getView();
    }

    @Override
    public PixelMapElement doScreenShot() {
        return mProxyRenderView.doScreenShot();
    }

    @Override
    public void release() {
        mProxyRenderView.release();
    }
}