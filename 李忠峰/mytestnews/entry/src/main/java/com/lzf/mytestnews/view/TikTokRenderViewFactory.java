package com.lzf.mytestnews.view;


import com.yc.video.surface.InterSurfaceView;
import com.yc.video.surface.RenderSurfaceView;
import com.yc.video.surface.SurfaceFactory;
import ohos.app.Context;

/**
 * TikTokRenderViewFactory
 *
 * @since 2021-05-12
 */
public class TikTokRenderViewFactory extends SurfaceFactory {
    /**
     * 创建
     *
     * @return TikTokRenderViewFactory
     */
    public static TikTokRenderViewFactory create() {
        return new TikTokRenderViewFactory();
    }

    @Override
    public InterSurfaceView createRenderView(Context context) {
        return new TikTokRenderView(new RenderSurfaceView(context));
    }
}
