package com.lzf.mytestnews.view;

import com.lzf.mytestnews.ResourceTable;
import com.lzf.mytestnews.utils.DialogUtils;
import com.yc.kernel.utils.VideoLogUtils;
import com.yc.music.utils.MusicLogUtils;
import com.yc.video.bridge.ControlWrapper;
import com.yc.video.config.ConstantKeys;
import com.yc.video.ui.view.InterControlView;
import ohos.agp.animation.Animator;
import ohos.agp.components.*;
import ohos.app.Context;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

/**
 * TikTokView
 *
 * @since 2021-05-12
 */
public class TikTokView extends StackLayout implements InterControlView, Component.TouchEventListener {
    private static final int DEFAULT_VALUE = 32;
    private Image thumb;
    private Image mPlayBtn;
    private ControlWrapper mControlWrapper;
    private int mScaledTouchSlop;
    private int mStartX;
    private int mStartY;
    private int playState = -1;

    /**
     * TikTokView
     *
     * @param context context
     */
    public TikTokView(Context context) {
        super(context);
    }

    /**
     * TikTokView
     *
     * @param context context
     * @param attrs attrs
     */
    public TikTokView(Context context, AttrSet attrs) {
        super(context, attrs);
    }

    /**
     * TikTokView
     *
     * @param context context
     * @param attrs attrs
     * @param defStyleAttr defStyleAttr
     */
    public TikTokView(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    {
        LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_layout_tiktok_controller, this, true);
        thumb = (Image) findComponentById(ResourceTable.Id_iv_thumb);
        mPlayBtn = (Image) findComponentById(ResourceTable.Id_play_btn);

        // 双击
        setDoubleClickedListener(new DoubleClickedListener() {
            @Override
            public void onDoubleClick(Component component) {
                if (mControlWrapper.getToggleState() == 1) { // 等于0点击页面就暂停或者播放
                    mControlWrapper.togglePlay();
                }
            }
        });
        mScaledTouchSlop = DEFAULT_VALUE;
        setTouchEventListener(this);
    }

    /**
     * 解决点击和VerticalViewPager滑动冲突问题
     *
     * @param component component
     * @param event event
     * @return return
     */
    @Override
    public boolean onTouchEvent(Component component, TouchEvent event) {
        int action = event.getAction();
        MmiPoint pointerPosition = event.getPointerPosition(event.getIndex());
        switch (action) {
            case TouchEvent.PRIMARY_POINT_DOWN:
                mStartX = (int) pointerPosition.getX();
                mStartY = (int) pointerPosition.getY();
                return true;
            case TouchEvent.OTHER_POINT_UP:
                int endX = (int) pointerPosition.getX();
                int endY = (int) pointerPosition.getY();
                if (Math.abs(endX - mStartX) < mScaledTouchSlop
                    && Math.abs(endY - mStartY) < mScaledTouchSlop) {
                    simulateClick();
                }
                break;
            case TouchEvent.PRIMARY_POINT_UP:
                if (mControlWrapper.getToggleState() != 1) { // 等于0点击页面就暂停或者播放
                    mControlWrapper.togglePlay();
                } else {
                    // 等于1就 显示或者隐藏进度条
                    mControlWrapper.toggleShowState();
                }
                MusicLogUtils.i("aaa PRIMARY_POINT_UP");
                break;
            default:
                break;
        }
        return false;
    }

    @Override
    public void attach(ControlWrapper controlWrapper) {
        mControlWrapper = controlWrapper;
    }

    @Override
    public Component getView() {
        return this;
    }

    @Override
    public void onVisibilityChanged(boolean isVisible, Animator anim) {
    }

    @Override
    public void onPlayStateChanged(int playState) {
        this.playState = playState;
        switch (playState) {
            case ConstantKeys.CurrentState.STATE_IDLE:
                VideoLogUtils.e("STATE_IDLE " + hashCode());
                thumb.setVisibility(VISIBLE);
                break;
            case ConstantKeys.CurrentState.STATE_PLAYING:
                VideoLogUtils.e("STATE_PLAYING " + hashCode());
                thumb.setVisibility(HIDE);
                mPlayBtn.setVisibility(HIDE);
                break;
            case ConstantKeys.CurrentState.STATE_PAUSED:
                VideoLogUtils.e("STATE_PAUSED " + hashCode());
                thumb.setVisibility(HIDE);
                if (mControlWrapper.getToggleState() != 1) {
                    mPlayBtn.setVisibility(VISIBLE);
                }
                break;
            case ConstantKeys.CurrentState.STATE_PREPARING:
                mPlayBtn.setVisibility(HIDE);
                VideoLogUtils.e("STATE_PREPARING " + hashCode());
                break;
            case ConstantKeys.CurrentState.STATE_PREPARED:
                VideoLogUtils.e("STATE_PREPARED " + hashCode());
                break;
            case ConstantKeys.CurrentState.STATE_ERROR:
                VideoLogUtils.e("STATE_ERROR " + hashCode());
                break;
            default:
                break;
        }
    }

    @Override
    public void onPlayerStateChanged(int playerState) {
        switch (playerState) {
            case ConstantKeys.PlayMode.MODE_FULL_SCREEN:
                mPlayBtn.setVisibility(HIDE);
                break;
            case ConstantKeys.PlayMode.MODE_NORMAL:
                if (playState == ConstantKeys.CurrentState.STATE_PAUSED) {
                    mPlayBtn.setVisibility(VISIBLE);
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void setProgress(int duration, int position) {
    }

    @Override
    public void onLockStateChanged(boolean isLocked) {
    }
}
