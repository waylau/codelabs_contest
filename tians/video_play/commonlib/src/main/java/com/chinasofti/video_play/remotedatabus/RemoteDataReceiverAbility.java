package com.chinasofti.video_play.remotedatabus;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.event.notification.NotificationRequest;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.rpc.IRemoteBroker;
import ohos.rpc.IRemoteObject;
import ohos.rpc.MessageOption;
import ohos.rpc.MessageParcel;
import ohos.rpc.RemoteObject;

import java.util.HashMap;
import java.util.Map;

/**
 * when remote device connect TV, it will connect with NewsServiceAbility instance.
 * if connection is done, we will return a RemoteMessage object to remote caller as
 * a proxy to communicate with native PA.
 * In this template, when a phone transfer news to A TV, the phone will try to start
 * this ability by calling connectAbility() at phone's DistributeInternalAbility.java.
 */
public class RemoteDataReceiverAbility extends Ability {
    private static final HiLogLabel TAG = new HiLogLabel(0, 0, "RemoteDataReceiverAbility");
    private static final int NOTIFICATION_ID = 1005;
    private static final Map<Integer, RemoteMessageHandler> HANDLER_MAP = new HashMap<>();
    private final RemoteHandler remote = new RemoteHandler();

    /**
     * when devices' FA need to subscribe from PA, it will invoke this method to register message handler.
     *
     * @param code    device message code.
     * @param handler message handler.
     */
    public static void registerMessageHandler(int code, RemoteMessageHandler handler) {
        HiLog.info(TAG, "registerMessageHandler code " + code);
        HANDLER_MAP.put(code, handler);
    }

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        // when transfer done create notification，notificationId is 1005.
        NotificationRequest request = new NotificationRequest(NOTIFICATION_ID);
        NotificationRequest.NotificationNormalContent content = new NotificationRequest.NotificationNormalContent();
        content.setTitle("News share").setText("News is connected with remote device");
        NotificationRequest.NotificationContent notificationContent =
                new NotificationRequest.NotificationContent(content);
        request.setContent(notificationContent);
        keepBackgroundRunning(NOTIFICATION_ID, request);
    }

    @Override
    protected IRemoteObject onConnect(Intent intent) {
        super.onConnect(intent);
        return remote.asObject();
    }

    @Override
    protected void onDisconnect(Intent intent) {
        super.onDisconnect(intent);
    }

    @Override
    protected void onStop() {
        super.onStop();
        cancelBackgroundRunning();
    }

    /**
     * the handler interface for FA subscribe PA mechanism's messageQueue.
     */
    public interface RemoteMessageHandler {
        void handlerRemote(MessageParcel data, MessageParcel reply);
    }

    /**
     * inner class RemoteMessage stands for proxy of remote device, we can communication
     * with remote device through this proxy
     */
    public static class RemoteHandler extends RemoteObject implements IRemoteBroker {
        private static final int ERROR = -1;

        /**
         * default constructor
         */
        RemoteHandler() {
            super("MyService_MessageRemote");
        }

        @Override
        public IRemoteObject asObject() {
            return this;
        }

        @Override
        public boolean onRemoteRequest(int code, MessageParcel data, MessageParcel reply, MessageOption option) {
            HiLog.info(TAG, "onRemoteRequest code " + code);
            if (HANDLER_MAP.get(code) == null) {
                reply.writeInt(ERROR);
                return false;
            }

            RemoteMessageHandler handler = HANDLER_MAP.get(code);
            handler.handlerRemote(data, reply);
            return true;
        }
    }

}