package com.chinasofti.video_play.newsdata;

import java.util.HashMap;
import java.util.Map;

import ohos.ace.ability.AceInternalAbility;
import ohos.app.AbilityContext;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.rpc.MessageOption;
import ohos.rpc.MessageParcel;
import ohos.utils.zson.ZSONArray;
import ohos.utils.zson.ZSONObject;

/**
 * NewsInfoInternalAbility mainly duty is to react for FA's news requests.
 * both phone and TV will get news list and news content from list PA.
 */
public class NewsInfoInternalAbility extends AceInternalAbility {
    private static final HiLogLabel TAG = new HiLogLabel(0, 0, "NewsInfoInternalAbility");
    private static final int ACTION_REQUEST_NEWS_LIST = 1001;
    private static final int ACTION_REQUEST_NEWS = 1002;
    private static NewsInfoInternalAbility INSTANCE;
    private final AbilityContext context;

    private NewsInfo newsInfo;

    /**
     * default constructor
     *
     * @param context ability context
     */
    public NewsInfoInternalAbility(AbilityContext context) {
        super("com.chinasofti.video_play.newsdata", "NewsInfoInternalAbility");
        this.context = context;
    }

    /**
     * set InternalAbilityHandler for NewsInfoInternalAbility instance
     *
     * @param context ability context
     */
    public static void register(AbilityContext context) {
        INSTANCE = new NewsInfoInternalAbility(context);
        INSTANCE.setInternalAbilityHandler((code, data, reply, option) ->
                INSTANCE.onRemoteRequest(code, data, reply, option));
    }

    /**
     * FA requests news list and news details from this PA with ACTION_CODE, it'll answer for both TV FA and phone FA.
     * phone/index.js,detail.js tv/index.js will get news content through this method.
     *
     * @param code   ACTION_CODE
     * @param data   data sent from FA
     * @param reply  reply to send back to FA
     * @param option currently excessive
     * @return whether request is correctly responded
     */
    private boolean onRemoteRequest(int code, MessageParcel data, MessageParcel reply, MessageOption option) {
        newsInfo = NewsInfo.getInstance(context);
        if (code == ACTION_REQUEST_NEWS_LIST) {
            ZSONArray zsonArray = new ZSONArray();
            int cnt = newsInfo.getNewsCount();
            for (int index = 0; index < cnt; index++) {
                String title = newsInfo.getNewsTitle(index);
                String url = newsInfo.getNewsUrl(index);
                String image = newsInfo.getNewsImage(index);
                boolean containsVideo = newsInfo.getNewsContainsVideos(index);
                String videoUrl = newsInfo.getNewsVideoUrl(index);
                boolean containsWeb = newsInfo.getNewsContainsWeb(index);
                String webPage = newsInfo.getWebNewsUrl(index);

                ZSONObject obj = new ZSONObject();
                obj.put("url", url);
                obj.put("title", title);
                obj.put("title_image", image);
                obj.put("contains_video", containsVideo);
                obj.put("video_url", videoUrl);
                obj.put("contains_webpage", containsWeb);
                obj.put("web_page", webPage);
                zsonArray.add(obj);
            }
            Map<String, Object> zsonResult = new HashMap<>();
            zsonResult.put("code", 0);
            zsonResult.put("list", zsonArray);
            reply.writeString(ZSONObject.toZSONString(zsonResult));
        } else if (code == ACTION_REQUEST_NEWS) {
            String zsonStr = data.readString();
            ZSONObject zsonObject = ZSONObject.stringToZSON(zsonStr);
            String content = newsInfo.getNewsJson(zsonObject.getString("url"));
            String index = zsonObject.getString("index");
            boolean containsVideo = newsInfo.getNewsContainsVideos(Integer.parseInt(index));
            String videoUrl = newsInfo.getNewsVideoUrl(Integer.parseInt(index));
            Map<String, Object> zsonResult = new HashMap<>();
            zsonResult.put("code", 0);
            zsonResult.put("contains_video", containsVideo);
            zsonResult.put("video_url", videoUrl);
            zsonResult.put("data", content);
            reply.writeString(ZSONObject.toZSONString(zsonResult));
        } else {
            HiLog.info(TAG, "Unsupported code from js");
        }
        return true;
    }
}
