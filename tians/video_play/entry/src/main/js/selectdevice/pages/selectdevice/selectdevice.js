import device from '@system.device';
import app from '@system.app';

const ACTION_REQUEST_DEVICE_LIST = 1001;

export default {
    data: {
        title: '',
        devices: new Array(),
        currentSelect: 0,
        appName: app.getInfo().appName,
        deviceIcon: {
            "SMART_PHONE": "common/images/ic_device_phone.png",
            "SMART_TV": "common/images/ic_device_tv.png",
            "LAPTOP": "common/images/ic_device_matebook.png",
            "SMART_PAD": "common/images/ic_pad.png",
            "SOUND": "common/images/ic_sound.png",
            "SOUNDX": "common/images/ic_soundx.png",
            "WATCH": "common/images/ic_watch.png"
        },
    },
    async onInit() {
        this.devices.push({
            name: this.$t('strings.native'),
            type: 0,
            checked: true,
            icon: "common/images/ic_device_phone.png",
            id: "",
            debugMode: true
        })
        await this.requestDevice();
    },
    makeAction(code, data) {
        let action = {};
        action.bundleName = "com.chinasofti.video_play.phone";
        action.abilityName = "DeviceSelectInternalAbility";
        action.messageCode = code;
        action.abilityType = 1;
        action.data = data;
        return action;
    },
    async requestDevice() {
        let action = this.makeAction(ACTION_REQUEST_DEVICE_LIST, {});
        try {
            var result = await FeatureAbility.callAbility(action);
            var resData = JSON.parse(result);
            if (resData.code == 0) {
                var deviceList = resData.devices;
                for (var i = 0; i < deviceList.length; i++) {
                    var deviceInfo = {};
                    deviceInfo.type = deviceList[i].type;
                    deviceInfo.name = deviceList[i].name;
                    deviceInfo.id = deviceList[i].id;
                    deviceInfo.icon = this.getDeviceIcon(deviceInfo.type);
                    deviceInfo.debugMode = deviceList[i].debugMode;
                    deviceInfo.checked = false;
                    this.devices.push(deviceInfo);
                }
            } else {
                console.error("requestDevice fail");
            }
        } catch (e) {
            console.error("requestDevice caught exception" + e);
        }
    },
    getDeviceIcon(type) {
        return this.deviceIcon[type];
    },
    async select(index) {
        this.devices[this.currentSelect].checked = false;
        this.devices[index].checked = true;
        this.currentSelect = index;
        let request = {}
        request.result = {
            deviceId: this.devices[index].id,
            deviceName: this.devices[index].name,
            deviceType: this.devices[index].type,
            deviceDebugMode: this.devices[index].debugMode,
            newsImage: this.transferIcon,
            newsTitle: this.title
        };
        var forwardCompatible = false;
        device.getInfo({
            success: function (data) {
                if (data.apiVersion >= 5) {
                    forwardCompatible = false;
                } else {
                    forwardCompatible = true;
                }
            },
            fail: function (data, code) {
                console.error("selectdevice.js get Device API version failed");
            }
        });
        var res;
        if (!!forwardCompatible == true) {
            request.code = 0;
            res = await FeatureAbility.finishWithResult(request);
        } else {
            res = await FeatureAbility.finishWithResult(0, request);
        }
        if (res.code != 0) {
            console.error("Select device fail" + res.code);
        }
    }
}