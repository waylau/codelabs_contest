package com.chinasofti.video_play;

import com.chinasofti.video_play.constant.Command;
import com.chinasofti.video_play.remotedatabus.RemoteDataSender;

import ohos.ace.ability.AceInternalAbility;
import ohos.app.AbilityContext;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.rpc.MessageOption;
import ohos.rpc.MessageParcel;
import ohos.rpc.RemoteException;
import ohos.utils.zson.ZSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * DistributeInternalAbility responsible for connect with remote device, also handle FA request : declare different
 * business logic to react according to messageCode sent from FA.
 */
public class DistributeInternalAbility extends AceInternalAbility {
    private static final HiLogLabel TAG = new HiLogLabel(0, 0, "DistributeInternalAbility");
    private static final int CONNECT_ABILITY = 2000;
    private static final int DISCONNECT_ABILITY = 2001;
    private static final int SEND_MSG = 2002;
    private static final int SUCCESS_CODE = 0;
    private static final int FAIL_CODE = -1;
    private static DistributeInternalAbility INSTANCE;
    private final AbilityContext context;
    private String selectDeviceId;

    /**
     * default constructor
     *
     * @param context ability context
     */
    public DistributeInternalAbility(AbilityContext context) {
        super("com.chinasofti.video_play.phone", "DistributeInternalAbility");
        this.context = context;
    }

    /**
     * setInternalAbilityHandler for DistributeInternalAbility instance
     *
     * @param context ability context
     */
    static void register(AbilityContext context) {
        INSTANCE = new DistributeInternalAbility(context);
        INSTANCE.setInternalAbilityHandler((code, data, reply, option) ->
                INSTANCE.onRemoteRequest(code, data, reply, option));
    }

    /**
     * destroy DistributeInternalAbility instance
     */
    private static void unregister() {
        INSTANCE.destroy();
    }

    /**
     * default destructor
     */
    private void destroy() {
    }

    /**
     * hand click request from javascript
     *
     * @param code   ACTION_CODE
     * @param data   data sent from javascript
     * @param reply  reply for javascript
     * @param option currently excessive
     * @return whether javascript click event is correctly responded
     */
    private boolean onRemoteRequest(int code, MessageParcel data, MessageParcel reply, MessageOption option) {
        Map<String, Object> replyResult = new HashMap<>();
        switch (code) {
            // send message to remote device, message contains controller command from FA
            case SEND_MSG: {
                try {
                    ZSONObject dataParsed = ZSONObject.stringToZSON(data.readString());
                    int message = dataParsed.getIntValue("message");
                    int success = RemoteDataSender.sendMessage(selectDeviceId, Command.COMMAND_TV, message);
                    if (success == -1) {
                        boolean connectRes = RemoteDataSender.connectRemote(selectDeviceId, context);
                        if (!connectRes) {
                            throw new RemoteException();
                        }
                    }
                    assembleReplyResult(SUCCESS_CODE, replyResult, new Object(), reply);
                } catch (RemoteException e) {
                    HiLog.error(TAG, "DistributeInternalAbility Exception " + e);
                }
                break;
            }
            // to invoke remote device's newsShare ability and send news url we transfer
            case CONNECT_ABILITY: {
                ZSONObject dataParsed = ZSONObject.stringToZSON(data.readString());
                selectDeviceId = dataParsed.getString("deviceId");
                boolean result = RemoteDataSender.connectRemote(selectDeviceId, context);
                assembleReplyResult(result ? SUCCESS_CODE : FAIL_CODE, replyResult, new Object(), reply);
                break;
            }
            // when controller FA went to destroy lifeCycle, disconnect with remote newsShare ability
            case DISCONNECT_ABILITY: {
                RemoteDataSender.disConnectRemote(selectDeviceId, context);
                assembleReplyResult(SUCCESS_CODE, replyResult, new Object(), reply);
                unregister();
                break;
            }
            default:
                HiLog.error(TAG, "messageCode not handle properly in phone distributeInternalAbility");
        }
        return true;
    }

    private void assembleReplyResult(int code, Map<String, Object> replyResult, Object content, MessageParcel reply) {
        replyResult.put("code", code);
        replyResult.put("content", content);
        reply.writeString(ZSONObject.toZSONString(replyResult));
    }
}
