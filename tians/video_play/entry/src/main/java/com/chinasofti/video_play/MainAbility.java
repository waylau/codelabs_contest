package com.chinasofti.video_play;

import com.chinasofti.video_play.newsdata.NewsInfoInternalAbility;

import ohos.aafwk.content.Intent;
import ohos.ace.ability.AceAbility;
import ohos.bundle.AbilityInfo;

/**
 * This template is a NewsShare demo to show the possibility of DistributeAbility of HarmonyOS.
 * The general business logic of this template is: when we read news on a phone we can transfer
 * specific news detail to SMART_TV or another phone, if the destination is TV, the phone will
 * router to controller page where we can send command to control news reading on the TV.
 * <p>
 * Currently our demo read news from json, you can rewrite get news method in NewsInfo.java to
 * choose different data source. And news content can also contains video you can extend this
 * template with more functions.
 * <p>
 * By the way, if you don't have a smart_TV as debug device, you can enable DEBUG_MODE below
 * to make your phone recognize your another phone as TV. You should install TV hap to your
 * another phone to make it as an TV instance, and install a "DEBUG_MODE" enabled hap to your
 * phone.
 */
public class MainAbility extends AceAbility {

    /**
     * If you need to simulate TV by your phone, please follow the instructions below.
     * 0.enable DEBUG_MODE and install this phone hap to your phone A.
     * 1.install TV hap to your phone B to make it a TV instance.
     *
     * @return DEBUG_MODE
     */
    static boolean enableDebugMode() {
        // default: true,which mean your another phone is simulated as TV.
        return true;
    }

    @Override
    public void onStart(Intent intent) {
        NewsInfoInternalAbility.register(this);
        DistributeInternalAbility.register(this);
        setDisplayOrientation(AbilityInfo.DisplayOrientation.PORTRAIT);
        setInstanceName("default");
        super.onStart(intent);
    }

    @Override
    public void onNewIntent(Intent intent) {
        if (intent.hasParameter("deviceId")) {
            pushPage("pages/remote_controller/controller", intent.getParams());
        }
        super.onNewIntent(intent);
    }

    @Override
    public void onStop() {
        super.onStop();
    }

}
