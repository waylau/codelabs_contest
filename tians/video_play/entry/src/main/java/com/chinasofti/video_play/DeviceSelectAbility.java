package com.chinasofti.video_play;

import ohos.aafwk.content.Intent;
import ohos.ace.ability.AceAbility;

/**
 * DeviceSelectAbility to invoke DeviceSelectInternalAbility.register() method
 */
public class DeviceSelectAbility extends AceAbility {
    @Override
    public void onStart(Intent intent) {
        DeviceSelectInternalAbility.register(this);
        setInstanceName("selectdevice");
        setPageParams("pages/selectdevice/selectdevice", intent.getParams());
        super.onStart(intent);
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}