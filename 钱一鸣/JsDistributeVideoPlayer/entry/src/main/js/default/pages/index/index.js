// @ts-nocheck
//xxx.js

import prompt from '@system.prompt';

var log = console.log.bind(console);

export default {
    data: {
        videoList: [
            {
                url: '/common/1.mov',
                starttime: 0,
                commentList: [
                    {comment: 'HDC2021 HarmonyOS'},
                    {comment: '好期待！！AVA'},
                    {comment: '大家好！我是向晚！！'},
                ],
            },
            {
                url: '/common/2.mov',
                starttime: 0,
                commentList: [
                    {comment: 'HDC2021 HarmonyOS'},
                    {comment: '好期待！！Queen'},
                    {comment: '大家好！我是乃琳！！是 Elin 哦～'},
                ],
            },
        ],
    },

    shareData: {
        inputValue: '',
        isStart: false,
        controls: true,
        currentIndex: 0,
    },

    tryContinueAbility: async function() {
        // 应用进行迁移
        let result = await FeatureAbility.continueAbility();
        log("tryContinueAbility result:" + JSON.stringify(result));
    },
    onStartContinuation() {
        // 判断当前的状态是不是适合迁移
        log("onStartContinuation");
        return true;
    },
    onSaveData(saveData) {
        // 数据保存到savedData中进行迁移。
        var data = {
            videoList: this.videoList,
        };
        Object.assign(saveData, data);
        log("onSaveData");
        log("data:" + JSON.stringify(data));
    },
    onCompleteContinuation(code) {
        // 迁移操作完成，code返回结果
        log("CompleteContinuation: code = " + code);
    },
    onRestoreData(restoreData) {
        // 收到迁移数据，恢复。
        log("onRestoreData: " + JSON.stringify(restoreData))
        this.videoList = restoreData.videoList;

        // 跳转到指定的视频
        this.$element('swiper').swipeTo({index: this.currentIndex});

        // 确定是否播放指定视频
        this.change_start_pause();
    },

    // 评论相关代码
    input_change(e){
        this.inputValue = e.value;
    },
    button_click(e){
        log("button_click: " + e.text)
        this.videoList[this.currentIndex].commentList.push({comment: this.inputValue});
        this.inputValue = '';
    },
    change_current_page(msg) {
        log("change_current_page: " + msg.direction + " " + msg.distance);
        switch (msg.direction) {
            case "left":
                this.$element('commentPanel').show();
                break;
            case "right":
                this.tryContinueAbility();
                log("TAG: " + "log in tryContinueAbility")
                break;
        }
    },
    close_pannel(msg) {
        if (msg.direction === "right") {
            log("close_pannel: " + msg.direction)
            this.$element('commentPanel').close();
        }
    },

    // 视频播放相关代码
    change_current_video(e) {
        log("change_current_video: " + e.index);
        this.currentIndex = e.index;
        if(this.currentIndex == 0) {
            this.$element('videoId1').pause();
            this.$element('videoId0').start();
        }
        if (this.currentIndex == 1) {
            this.$element('videoId0').pause();
            this.$element('videoId1').start();
        }
    },
    timeupdateCallback(e){
        this.videoList[this.currentIndex].starttime = e.currenttime;
    },
    change_start_pause() {
        if(this.isStart) {
            this.$element(`videoId${this.currentIndex}`).pause();
            this.isStart = false;
            this.controls = true;
        } else {
            log("change_start_pause: " + `videoId${this.currentIndex}`)
            this.$element(`videoId${this.currentIndex}`).start();
            this.isStart = true;
            this.controls = false;
        }
    },
}