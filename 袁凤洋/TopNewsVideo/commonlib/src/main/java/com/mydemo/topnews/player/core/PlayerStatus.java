package com.mydemo.topnews.player.core;

/**
 * Enum of playback status constants.
 */
public enum PlayerStatus {
    /**
     * the video is released
     */
    IDLE("idle"),

    /**
     * video is preparing
     */
    PREPARING("preparing"),

    /**
     * when the video become prepared will be ready to play
     */
    PREPARED("prepared"),

    /**
     * start the video or resume to play
     */
    PLAY("play"),

    /**
     * pause the playing
     */
    PAUSE("pause"),

    /**
     * stop the playing
     */
    STOP("stop"),

    /**
     * the video play completed
     */
    COMPLETE("complete"),

    /**
     * the wrong status of video
     */
    ERROR("error"),

    /**
     * before the status of play
     */
    BUFFERING("buffering");

    private final String status;

    PlayerStatus(String value) {
        this.status = value;
    }

    public String getStatus() {
        return status;
    }
}
