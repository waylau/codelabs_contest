package vip.qsos.hm.contest.player.api;

public interface ImplLifecycle {

    void onStart();

    void onForeground();

    void onBackground();

    void onStop();
}
