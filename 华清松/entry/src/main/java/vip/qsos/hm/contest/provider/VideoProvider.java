package vip.qsos.hm.contest.provider;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.PageSliderProvider;

import java.util.List;

public class VideoProvider<T extends Component> extends PageSliderProvider {
    private final List<T> componentList;

    public VideoProvider(List<T> componentList) {
        this.componentList = componentList;
    }

    public int getCount() {
        return componentList.size();
    }

    @Override
    public Object createPageInContainer(ComponentContainer componentContainer, int index) {
        componentContainer.addComponent(componentList.get(index));
        return componentList.get(index);
    }

    @Override
    public void destroyPageFromContainer(ComponentContainer componentContainer, int index, Object obj) {
        componentContainer.removeComponent(componentList.get(index));
    }

    @Override
    public boolean isPageMatchToObject(Component component, Object obj) {
        return component == obj;
    }
}
