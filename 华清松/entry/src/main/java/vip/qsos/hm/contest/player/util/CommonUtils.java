package vip.qsos.hm.contest.player.util;

import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import vip.qsos.hm.contest.player.constant.Constants;
import vip.qsos.hm.contest.util.LogUtil;

import java.io.IOException;

public class CommonUtils {
    private static final String TAG = CommonUtils.class.getSimpleName();

    private CommonUtils() {
    }

    public static int getColor(Context context, int resourceId) {
        try {
            return context.getResourceManager().getElement(resourceId).getColor();
        } catch (IOException | NotExistException | WrongTypeException e) {
            LogUtil.error(TAG, e.getMessage());
        }
        return Constants.NUMBER_NEGATIVE_1;
    }
}
