package vip.qsos.hm.contest.player.api;

public interface ImplPlayModule {

    void bind(ImplPlayer player);

    void unbind();
}
