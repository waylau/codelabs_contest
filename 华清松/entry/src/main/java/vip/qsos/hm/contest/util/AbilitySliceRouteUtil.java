package vip.qsos.hm.contest.util;

import ohos.aafwk.ability.AbilitySlice;

import java.util.ArrayList;
import java.util.List;

public class AbilitySliceRouteUtil {
    private static AbilitySliceRouteUtil instance;

    private final List<AbilitySlice> routes;

    private AbilitySliceRouteUtil() {
        routes = new ArrayList<>(0);
    }

    public static synchronized AbilitySliceRouteUtil getInstance() {
        if (instance == null) {
            instance = new AbilitySliceRouteUtil();
        }
        return instance;
    }

    public void addRoute(AbilitySlice slice) {
        routes.add(slice);
    }

    public void removeRoute(AbilitySlice slice) {
        for (int i = routes.size() - 1; i >= 0; i--) {
            if (routes.get(i).equals(slice)) {
                routes.remove(routes.get(i));
                break;
            }
        }
    }

    public void backTo(String sliceName) {
        List<AbilitySlice> slices = new ArrayList<>(0);
        for (int i = routes.size() - 1; i >= 0; i--) {
            if (routes.get(i).getClass().getName().equals(sliceName)) {
                break;
            } else {
                slices.add(routes.get(i));
            }
        }
        if (slices.size() > 0) {
            terminateSlices(slices);
        }
    }

    public void terminateSlices() {
        terminateSlices(routes);
    }

    private void terminateSlices(List<AbilitySlice> slices) {
        for (int i = slices.size() - 1; i >= 0; i--) {
            slices.get(i).terminate();
        }
    }
}
