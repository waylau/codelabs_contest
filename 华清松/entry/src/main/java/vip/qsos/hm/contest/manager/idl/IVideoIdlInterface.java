package vip.qsos.hm.contest.manager.idl;

import ohos.rpc.IRemoteBroker;
import ohos.rpc.RemoteException;

public interface IVideoIdlInterface extends IRemoteBroker {
    void flyIn(int index) throws RemoteException;

    void playControl(int controlCode, int extras) throws RemoteException;

    int flyOut() throws RemoteException;
}

