package vip.qsos.hm.contest.player.constant;

public enum PlayerStatus {
    IDEL,
    PREPARING,
    PREPARED,
    PLAY,
    PAUSE,
    STOP,
    COMPLETE,
    ERROR,
    BUFFERING
}
