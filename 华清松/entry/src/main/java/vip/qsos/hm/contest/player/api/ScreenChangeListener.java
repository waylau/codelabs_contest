package vip.qsos.hm.contest.player.api;

public interface ScreenChangeListener {
    void screenCallback(int width, int height);
}
