package vip.qsos.hm.contest.manager.idl;

import ohos.rpc.IRemoteObject;
import ohos.rpc.MessageOption;
import ohos.rpc.MessageParcel;
import ohos.rpc.RemoteException;

public class VideoMigrationProxy implements IVideoIdlInterface {
    private static final String DESCRIPTOR = "vip.qsos.hm.contest.IVideoIdlInterface";
    private static final int COMMAND_FLY_IN = IRemoteObject.MIN_TRANSACTION_ID;
    private static final int COMMAND_PLAY_CONTROL = IRemoteObject.MIN_TRANSACTION_ID + 1;
    private static final int COMMAND_FLY_OUT = IRemoteObject.MIN_TRANSACTION_ID + 2;

    private final IRemoteObject remote;

    public VideoMigrationProxy(IRemoteObject remote) {
        this.remote = remote;
    }

    @Override
    public IRemoteObject asObject() {
        return remote;
    }

    @Override
    public void flyIn(int index) throws RemoteException {
        MessageParcel data = MessageParcel.obtain();
        MessageParcel reply = MessageParcel.obtain();
        MessageOption option = new MessageOption(MessageOption.TF_SYNC);

        data.writeInterfaceToken(DESCRIPTOR);
        data.writeInt(index);

        try {
            remote.sendRequest(COMMAND_FLY_IN, data, reply, option);
            reply.readException();
        } finally {
            data.reclaim();
            reply.reclaim();
        }
    }

    @Override
    public void playControl(int controlCode, int extras) throws RemoteException {
        MessageParcel data = MessageParcel.obtain();
        MessageParcel reply = MessageParcel.obtain();
        MessageOption option = new MessageOption(MessageOption.TF_SYNC);

        data.writeInterfaceToken(DESCRIPTOR);
        data.writeInt(controlCode);
        data.writeInt(extras);

        try {
            remote.sendRequest(COMMAND_PLAY_CONTROL, data, reply, option);
            reply.readException();
        } finally {
            data.reclaim();
            reply.reclaim();
        }
    }

    @Override
    public int flyOut() throws RemoteException {
        MessageParcel data = MessageParcel.obtain();
        MessageParcel reply = MessageParcel.obtain();
        MessageOption option = new MessageOption(MessageOption.TF_SYNC);

        data.writeInterfaceToken(DESCRIPTOR);
        try {
            remote.sendRequest(COMMAND_FLY_OUT, data, reply, option);
            reply.readException();
            return reply.readInt();
        } finally {
            data.reclaim();
            reply.reclaim();
        }
    }
}

