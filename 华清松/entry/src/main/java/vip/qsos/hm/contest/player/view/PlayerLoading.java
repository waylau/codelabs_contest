package vip.qsos.hm.contest.player.view;

import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.*;
import ohos.agp.components.Component.TouchEventListener;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;
import vip.qsos.hm.contest.ResourceTable;
import vip.qsos.hm.contest.player.api.ImplPlayModule;
import vip.qsos.hm.contest.player.api.ImplPlayer;

public class PlayerLoading extends ComponentContainer implements ImplPlayModule, TouchEventListener {
    private static final int HALF_NUMBER = 2;
    private static final int ANIM_ROTATE = 360;
    private static final int ANIM_DURATION = 2000;
    private static final int ANIM_LOOPED_COUNT = -1;
    private ImplPlayer mPlayer;
    private Image mLoading;
    private AnimatorProperty mLoadingAnim;

    public PlayerLoading(Context context) {
        this(context, null);
    }

    public PlayerLoading(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    public PlayerLoading(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        initView(context);
    }

    private void initView(Context context) {
        Component loadingContainer =
                LayoutScatter.getInstance(context).parse(ResourceTable.Layout_player_loading_layout, null, false);
        if (loadingContainer.findComponentById(ResourceTable.Id_image_loading) instanceof Image) {
            mLoading = (Image) loadingContainer.findComponentById(ResourceTable.Id_image_loading);
            initAnim();
        }
        addComponent(loadingContainer);
        hide();
        setTouchEventListener(this);
    }

    private void initAnim() {
        int with = mLoading.getWidth() / HALF_NUMBER;
        int height = mLoading.getHeight() / HALF_NUMBER;
        mLoading.setPivotX(with);
        mLoading.setPivotY(height);
        mLoadingAnim = mLoading.createAnimatorProperty();
        mLoadingAnim.rotate(ANIM_ROTATE).setDuration(ANIM_DURATION).setLoopedCount(ANIM_LOOPED_COUNT);
    }

    private void initListener() {
        mPlayer.addPlayerStatusCallback(statu -> mContext.getUITaskDispatcher().asyncDispatch(() -> {
            switch (statu) {
                case PREPARING:
                case BUFFERING:
                    show();
                    break;
                case PLAY:
                    hide();
                    break;
                default:
                    break;
            }
        }));
    }

    public void show() {
        if (mLoadingAnim.isPaused()) {
            mLoadingAnim.resume();
        } else {
            mLoadingAnim.start();
        }
        setVisibility(VISIBLE);
    }

    public void hide() {
        setVisibility(INVISIBLE);
        mLoadingAnim.pause();
    }

    @Override
    public void bind(ImplPlayer player) {
        mPlayer = player;
        initListener();
    }

    @Override
    public void unbind() {
        mLoadingAnim.release();
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        return true;
    }
}
