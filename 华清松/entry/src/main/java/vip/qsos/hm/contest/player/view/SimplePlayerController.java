package vip.qsos.hm.contest.player.view;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;
import vip.qsos.hm.contest.ResourceTable;
import vip.qsos.hm.contest.player.api.ImplPlayModule;
import vip.qsos.hm.contest.player.api.ImplPlayer;
import vip.qsos.hm.contest.player.api.StatusChangeListener;
import vip.qsos.hm.contest.player.constant.Constants;
import vip.qsos.hm.contest.player.constant.PlayerStatus;
import vip.qsos.hm.contest.player.util.DateUtils;

public class SimplePlayerController extends ComponentContainer implements ImplPlayModule {
    private static final int THUMB_RED = 255;
    private static final int THUMB_GREEN = 255;
    private static final int THUMB_BLUE = 240;
    private static final int THUMB_WIDTH = 40;
    private static final int THUMB_HEIGHT = 40;
    private static final int THUMB_RADIUS = 20;
    private static final int CONTROLLER_HIDE_DELAY_TIME = 5000;
    private static final int PROGRESS_RUNNING_TIME = 1000;
    private boolean mIsDragMode = false;
    private final Context mContext;
    private ImplPlayer mPlayer;
    private DirectionalLayout bottomLayout;
    private Image mPlayToole;
    private Image mForward;
    private Image mBackward;
    private Slider mProgressBar;
    private Text mCurrentTime;
    private Text mTotalTime;
    private ControllerHandler mHandler;
    private final StatusChangeListener mStatusChangeListener = new StatusChangeListener() {
        @Override
        public void callback(PlayerStatus status) {
            mContext.getUITaskDispatcher().asyncDispatch(() -> {
                switch (status) {
                    case PREPARING:
                        mPlayToole.setClickable(false);
                        mProgressBar.setEnabled(false);
                        mProgressBar.setProgressValue(0);
                        break;
                    case PREPARED:
                        mProgressBar.setMaxValue(mPlayer.getDuration());
                        mTotalTime.setText(DateUtils.msToString(mPlayer.getDuration()));
                        break;
                    case PLAY:
                        showController(false);
                        mPlayToole.setPixelMap(ResourceTable.Media_ic_music_stop);
                        mPlayToole.setClickable(true);
                        mProgressBar.setEnabled(true);
                        break;
                    case PAUSE:
                        mPlayToole.setPixelMap(ResourceTable.Media_ic_music_play);
                        break;
                    case STOP:
                    case COMPLETE:
                        mPlayToole.setPixelMap(ResourceTable.Media_ic_update);
                        mProgressBar.setEnabled(false);
                        break;
                    default:
                        break;
                }
            });
        }
    };

    public SimplePlayerController(Context context) {
        this(context, null);
    }

    public SimplePlayerController(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    public SimplePlayerController(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        mContext = context;
        createHandler();
        initView();
        initListener();
    }

    private void createHandler() {
        EventRunner runner = EventRunner.create(true);
        if (runner == null) {
            return;
        }
        mHandler = new ControllerHandler(runner);
    }

    private void initView() {
        Component playerController =
                LayoutScatter.getInstance(mContext)
                        .parse(ResourceTable.Layout_simple_player_controller_layout, null, false);
        addComponent(playerController);
        if (playerController.findComponentById(ResourceTable.Id_controller_bottom_layout)
                instanceof DirectionalLayout) {
            bottomLayout = (DirectionalLayout) playerController
                    .findComponentById(ResourceTable.Id_controller_bottom_layout);
        }
        if (playerController.findComponentById(ResourceTable.Id_play_controller) instanceof Image) {
            mPlayToole = (Image) playerController.findComponentById(ResourceTable.Id_play_controller);
        }
        if (playerController.findComponentById(ResourceTable.Id_play_forward) instanceof Image) {
            mForward = (Image) playerController.findComponentById(ResourceTable.Id_play_forward);
        }
        if (playerController.findComponentById(ResourceTable.Id_play_backward) instanceof Image) {
            mBackward = (Image) playerController.findComponentById(ResourceTable.Id_play_backward);
        }
        if (playerController.findComponentById(ResourceTable.Id_progress) instanceof Slider) {
            mProgressBar = (Slider) playerController.findComponentById(ResourceTable.Id_progress);
        }
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(new RgbColor(THUMB_RED, THUMB_GREEN, THUMB_BLUE));
        shapeElement.setBounds(0, 0, THUMB_WIDTH, THUMB_HEIGHT);
        shapeElement.setCornerRadius(THUMB_RADIUS);
        mProgressBar.setThumbElement(shapeElement);
        if (playerController.findComponentById(ResourceTable.Id_current_time) instanceof Text) {
            mCurrentTime = (Text) playerController.findComponentById(ResourceTable.Id_current_time);
        }
        if (playerController.findComponentById(ResourceTable.Id_end_time) instanceof Text) {
            mTotalTime = (Text) playerController.findComponentById(ResourceTable.Id_end_time);
        }
    }

    private void initListener() {
        bottomLayout.setTouchEventListener((component, touchEvent) -> true);
    }

    private void initPlayListener() {
        mPlayer.addPlayerStatusCallback(mStatusChangeListener);
        mPlayToole.setClickedListener(component -> {
            if (mPlayer.isPlaying()) {
                mPlayer.pause();
            } else {
                if (mPlayer.getPlayerStatus() == PlayerStatus.STOP) {
                    mPlayer.replay();
                } else {
                    mPlayer.resume();
                }
            }
        });
        mForward.setClickedListener(component ->
                mPlayer.rewindTo(getBasicTransTime(mPlayer.getCurrentPosition()) + Constants.REWIND_STEP));
        mBackward.setClickedListener(component ->
                mPlayer.rewindTo(getBasicTransTime(mPlayer.getCurrentPosition()) - Constants.REWIND_STEP));
        mProgressBar.setValueChangedListener(
                new Slider.ValueChangedListener() {
                    @Override
                    public void onProgressUpdated(Slider slider, int value, boolean isB) {
                        mContext.getUITaskDispatcher().asyncDispatch(() ->
                                mCurrentTime.setText(DateUtils.msToString(value)));
                    }

                    @Override
                    public void onTouchStart(Slider slider) {
                        mIsDragMode = true;
                        mHandler.removeEvent(Constants.PLAYER_PROGRESS_RUNNING, EventHandler.Priority.IMMEDIATE);
                    }

                    @Override
                    public void onTouchEnd(Slider slider) {
                        mIsDragMode = false;
                        if (slider.getProgress() == mPlayer.getDuration()) {
                            mPlayer.stop();
                        } else {
                            mPlayer.rewindTo(getBasicTransTime(slider.getProgress()));
                        }
                    }
                });
    }

    private int getBasicTransTime(int currentTime) {
        return currentTime / PROGRESS_RUNNING_TIME * PROGRESS_RUNNING_TIME;
    }

    public void showController(boolean isAutoHide) {
        mHandler.sendEvent(Constants.PLAYER_CONTROLLER_SHOW, EventHandler.Priority.HIGH);
        if (isAutoHide) {
            hideController(CONTROLLER_HIDE_DELAY_TIME);
        } else {
            mHandler.removeEvent(Constants.PLAYER_CONTROLLER_HIDE);
        }
    }

    public void hideController(int delay) {
        mHandler.removeEvent(Constants.PLAYER_CONTROLLER_HIDE);
        mHandler.sendEvent(Constants.PLAYER_CONTROLLER_HIDE, delay, EventHandler.Priority.HIGH);
    }

    @Override
    public void bind(ImplPlayer player) {
        mPlayer = player;
        initPlayListener();
    }

    @Override
    public void unbind() {
        mHandler.removeAllEvent();
        mHandler = null;
    }

    private class ControllerHandler extends EventHandler {
        private int currentPosition;

        private ControllerHandler(EventRunner runner) {
            super(runner);
        }

        @Override
        public void processEvent(InnerEvent event) {
            super.processEvent(event);
            if (event == null) {
                return;
            }
            switch (event.eventId) {
                case Constants.PLAYER_PROGRESS_RUNNING:
                    if (mPlayer != null && mPlayer.isPlaying() && !mIsDragMode) {
                        currentPosition = mPlayer.getCurrentPosition();
                        while (currentPosition < PROGRESS_RUNNING_TIME) {
                            currentPosition = mPlayer.getCurrentPosition();
                        }
                        mContext.getUITaskDispatcher().asyncDispatch(() -> {
                            mProgressBar.setProgressValue(currentPosition);
                            mCurrentTime.setText(DateUtils.msToString(currentPosition));
                        });
                        mHandler.sendEvent(
                                Constants.PLAYER_PROGRESS_RUNNING, PROGRESS_RUNNING_TIME, Priority.HIGH);
                    }
                    break;
                case Constants.PLAYER_CONTROLLER_HIDE:
                    mContext.getUITaskDispatcher().asyncDispatch(() -> setVisibility(INVISIBLE));
                    mHandler.removeEvent(Constants.PLAYER_PROGRESS_RUNNING);
                    break;
                case Constants.PLAYER_CONTROLLER_SHOW:
                    mHandler.removeEvent(Constants.PLAYER_PROGRESS_RUNNING);
                    mHandler.sendEvent(Constants.PLAYER_PROGRESS_RUNNING, Priority.IMMEDIATE);
                    mContext.getUITaskDispatcher().asyncDispatch(() -> {
                        if (getVisibility() != VISIBLE) {
                            setVisibility(VISIBLE);
                        }
                    });
                    break;
                default:
                    break;
            }
        }
    }
}
