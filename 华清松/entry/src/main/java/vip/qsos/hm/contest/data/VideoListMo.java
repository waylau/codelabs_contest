package vip.qsos.hm.contest.data;

public class VideoListMo {
    private int sourceId;
    private String name;
    private String description;

    public VideoListMo(int sourceId, String name, String description) {
        this.sourceId = sourceId;
        this.name = name;
        this.description = description;
    }

    public int getSourceId() {
        return sourceId;
    }

    public void setSourceId(int sourceId) {
        this.sourceId = sourceId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
