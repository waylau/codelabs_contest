package vip.qsos.hm.contest.player.manager;

import vip.qsos.hm.contest.player.api.ImplLifecycle;
import vip.qsos.hm.contest.player.api.ImplPlayer;
import vip.qsos.hm.contest.player.constant.PlayerStatus;

public class HmPlayerLifecycle implements ImplLifecycle {
    private final ImplPlayer mPlayer;

    public HmPlayerLifecycle(ImplPlayer player) {
        mPlayer = player;
    }

    @Override
    public void onStart() {
    }

    @Override
    public void onForeground() {
        String url = mPlayer.getBuilder().getFilePath();
        int startMillisecond = mPlayer.getBuilder().getStartMillisecond();
        mPlayer.reload(url, startMillisecond);
    }

    @Override
    public void onBackground() {
        mPlayer.getBuilder().setPause(mPlayer.getPlayerStatus() == PlayerStatus.PAUSE);
        mPlayer.getBuilder().setStartMillisecond(mPlayer.getCurrentPosition());
        mPlayer.release();
    }

    @Override
    public void onStop() {
        mPlayer.release();
    }
}
