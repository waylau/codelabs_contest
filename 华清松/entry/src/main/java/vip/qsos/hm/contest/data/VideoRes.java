package vip.qsos.hm.contest.data;

import java.util.ArrayList;

public class VideoRes {
    private int sourceId;
    private String sourceUrl;
    private String description;
    private ArrayList<String> comments = new ArrayList<>();

    public VideoRes(int sourceId, String sourceUrl, String description) {
        this.sourceId = sourceId;
        this.sourceUrl = sourceUrl;
        this.description = description;
    }

    public int getSourceId() {
        return sourceId;
    }

    public void setSourceId(int sourceId) {
        this.sourceId = sourceId;
    }

    public String getSourceUrl() {
        return sourceUrl;
    }

    public void setSourceUrl(String sourceUrl) {
        this.sourceUrl = sourceUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<String> getComments() {
        return comments;
    }

    public void setComments(ArrayList<String> comments) {
        this.comments = comments;
    }
}
