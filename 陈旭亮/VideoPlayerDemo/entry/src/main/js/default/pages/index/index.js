import app from '@system.app';

export default {
    data: {
        autoplay: false, // 是否自动播放
        controlShow: false, // 是否显示控制栏
        loop: false, // 是否循环播放
        startTime: 0, // 播放开始时间
        speed: 1.0, // 播放速度
        lastIndex: 0,
        currentIndex: 0,
        //数据源
        urllist: [{
                      id: 0,
                      url: '/common/videos/1.mp4',
                      posterUrl: '/common/images/1.jpg',
                      title: '烟花',
                      detail: '烟花真好看……',
                      commentList: [{
                                        people: "小红",
                                        comment: "Harmony 真好"
                                    }, {
                                        people: "小花",
                                        comment: "Harmony 特别好"
                                    }]
                  }, {
                      id: 1,
                      url: '/common/videos/2.mp4',
                      posterUrl: '/common/images/2.jpg',
                      title: '跳楼机',
                      detail: '跳楼机有点厉害啊^-^',
                      commentList: [{
                                        people: "小白",
                                        comment: "Harmony good"
                                    }, {
                                        people: "小蓝",
                                        comment: "Harmony very good"
                                    }]
                  }, {
                      id: 2,
                      url: '/common/videos/3.mp4',
                      posterUrl: '/common/images/3.jpg',
                      title: '好风景',
                      detail: '大江大河好风景啊',
                      commentList: [{
                                        people: "小小",
                                        comment: "Harmony 真好 真好"
                                    }, {
                                        people: "小黄",
                                        comment: "Harmony 特别好 特别好"
                                    }]
                  }],
        commentShow: false, //控制评论区的显示
        currentComment: '', //当前输入的评论
    },
    //切换页面的回调
    onChange: function (e) {
        console.info("tag001", "onChange,e" + JSON.stringify(e))
        this.lastIndex = this.currentIndex;
        this.currentIndex = e.index;
        if (this.currentIndex != this.lastIndex) {
            //this.$element('videoId-' + this.lastIndex).stop();//6+
            this.$element('videoId-' + this.lastIndex).pause();
        }
        this.$element('videoId-' + this.currentIndex).setCurrentTime({
            currenttime: 0
        })
        this.$element('videoId-' + this.currentIndex).start();
    },
    onSwipe: function (e) {
        console.info("tag001", "onSwipe,e:" + JSON.stringify(e));
    },

    //播放器准备完成
    onPrepared: function (item) {
        console.info("tag001", "onPrepared,item:" + JSON.stringify(item));
        if (this.currentIndex == item.id) {
            this.$element('videoId-' + this.currentIndex).start();
        }
    },

    //发送评论
    enterkeyClick: function (e) {
        this.sendComment();
    },
    buttonClick: function (e) {
        this.sendComment();
    },
    sendComment: function () {
        if (this.currentComment == '') {
            return;
        }
        let size = this.urllist[this.currentIndex].commentList.length;
        let comment = {
            people: "打工人",
            comment: this.currentComment
        }
        this.urllist[this.currentIndex].commentList.splice(size, 0, comment);
    },
    onInputChange: function (e) {
        this.currentComment = e.value;
    },
    gotoComment: function (e) {
        this.commentShow = true;
    },
    closeComment: function (e) {
        this.commentShow = false;
    },

    //应用迁移
    doDistribute: function (e) {
        this.tryContinueAbility();
    },
    tryContinueAbility: async function () {
        // 应用进行迁移
        let result = await FeatureAbility.continueAbility();
        console.info("tag001", "result:" + JSON.stringify(result));
    },
    onStartContinuation() {
        // 判断当前的状态是不是适合迁移
        console.info("tag001", "onStartContinuation");
        return true;
    },
    onCompleteContinuation(code) {
        // 迁移操作完成，code返回结果
        console.info("tag001", "CompleteContinuation: code = " + code);
        app.terminate();
    },
    onSaveData(saveData) {
        // 数据保存到savedData中进行迁移。
        var data = {
            currentIndex: this.currentIndex,
            lastIndex: this.lastIndex
        };
        Object.assign(saveData, data)
    },
    onRestoreData(restoreData) {
        // 收到迁移数据，恢复。
        this.currentIndex = restoreData.currentIndex;
        this.lastIndex = restoreData.lastIndex;
        console.info("tag001", "onRestoreData,restoreData:" + JSON.stringify(restoreData));
        if (this.currentIndex > 0) {
            this.$element("swiper").swipeTo({
                index: this.currentIndex
            });
        }
    },
}
