package com.example.codelablechallenge02;

import ohos.ace.ability.AceAbility;
import ohos.aafwk.content.Intent;
import ohos.bundle.IBundleManager;

public class MainAbility extends AceAbility {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        if(verifySelfPermission("ohos.permission.DISTRIBUTED_DATASYNC")!= IBundleManager.PERMISSION_GRANTED){
            if (canRequestPermission("ohos.permission.DISTRIBUTED_DATASYNC")){
                requestPermissionsFromUser(new String[]{"ohos.permission.DISTRIBUTED_DATASYNC"},0);
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
