### **介绍：**HarmonyOS线上Codelabs挑战赛作品提交仓库



感谢您参与HarmonyOS线上Codelabs挑战赛

#### **作品提交方式：**

此次比赛采用向此仓库提交Pull Request的方式来收集参赛作品，Pull Request请求被合并入仓库后则表示作品提交成功。具体方法请参考[如何在Gitee提交Pull Request](https://gitee.com/help/articles/4128)

#### **作品提交需求：**

此次比赛作品包括

1、app源码

2、作品演示图片/GIF/视频

以“姓名”命名文件夹，将以上文件打包整理放入文件夹内，并提交至对应目录下。

请遵循开源版本Apache 2.0的规范，提前对贡献内容自查是否符合开源要求，例如：是否添加开源许可证，代码文件是否添加版权头说明等。

如果您已完成作品提交，请填写[问卷](http://hwdeveloper.wjx.cn/vj/efHI6WX.aspx?udsid=927339)反馈您的信息。

请在[帖子](http://developer.huawei.com/consumer/cn/forum/topic/0202691608475740730?fid=0101303901040230869)跟帖回复撰写的文章链接。